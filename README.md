## Requirements
* [Install MongoDB](https://www.mongodb.com/download-center#community)
* [Install Robo 3T](https://robomongo.org/) (formerly Robomongo)


## Notes
Currently using GraphiQL UI to mutate and query, should be using a client such as [React Apollo](https://github.com/apollographql/react-apollo) to do so for a real app.

GraphQL and Express don't know how to work together. This is where "apollo-server-express" comes into act as the glue between the two so that they can work together.

Mutation has to be the same in `schema.js` as `resolver.js`.



## GraphQL Queries

Go to GraphiQL - [http://localhost:4000/graphiql](http://localhost:4000/graphiql)

## Show all data
```
{
  authors {
    id
    name
    age
    books
  }
}
```

## Mutation example

Add / Update / Delete
```
mutation {
  doSomething(key: value)
}
```

Add / Update / Delete then show result
mutation {
  doSomething(key1: value1, key2: value2, key3: ["arrItem1", "arrItem2"]) {
    key1
    key2
    key3
  }
}

## Mutation - add author
```
mutation {
  addAuthor(name: "JK Rowling", age: 50, books:["Harry Potter 1", "Harry Potter 2"]) {
    id
    name
    age
    books
  }
}
```


## Mutation - delete author (by ID)
```
mutation {
  deleteAuthor(id: "7b84e970-004d-11e8-b6cd-832a045c6a65") {
    name
  }
}
```

## Mutation - update (by ID)
Updates name

```
mutation {
  updateAuthor(id: "91a7a6a0-004f-11e8-92a2-a1013c4972f2", name: "George RR Martin") {
    name
  }
}
```

## Selectively query

Query by age, return name of author/s
```
{
  authors(age: 50) {
    name
  }
}
```

Query one author by ID, get their name
```
{
  author(id: "72c5cef0-0050-11e8-a387-fd9553607fe0") {
    name
  }
}
```