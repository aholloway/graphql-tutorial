import mongoose from 'mongoose';
import uuid from 'uuid';

const schema = mongoose.Schema;

// create a new author schema
// Mongoose schema needs to be the same as GraphQL in schema.js
const authorSchema = new schema({
  id: {type: String, default: uuid.v1}, // ID auto-generated by UUID NPM module
  name: String,
  age: Number,
  books: [String]
});

const model = mongoose.model('author', authorSchema);
export default model;