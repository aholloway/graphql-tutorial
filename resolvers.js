import mongoose from 'mongoose';
import authorModel from './models/author';

// Structure needs to match the Type Definitions defined in schema.js
const resolvers = {
  Query: {
    authors: (root, {age}) => {
      // return authorModel.find({}); // return all authors
      return authorModel.find({age: age}); // return all users find by age (age required to be passed in)
    },
    author: (root, {id}) => {
      return authorModel.findOne({id: id}); // find and return one author based on their ID
    }
  },
  Mutation: {
    addAuthor: (root, {name, age, books}) => {
      const author = new authorModel({age: age, name: name, books: books});
      return author.save();
    },
    deleteAuthor: (root, {id}) => {
      return authorModel.findOneAndRemove({id: id})
    },
    updateAuthor: (root, {id, name}) => {
      return authorModel.findOneAndUpdate({id: id}, {name: name});
    }
  }
}

export default resolvers;